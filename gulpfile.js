const gulp = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const uglify = require("gulp-uglify");
const browserify = require("browserify");
const source = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");
const babelify = require("babelify");
const browserifyReplace = require("browserify-replace");
const clean = require("gulp-clean");
const browserSync = require("browser-sync");
const replace = require("gulp-replace");
const args = require("args-parser")(process.argv);
const gulpIf = require("gulp-if");
const { rootPath } = require("get-root-path");
const fs = require("fs");
const path = require("path");

const isProduction = args.production || false;

/**
 * Wczytywanie konfiguracji dla skryptu budującego grę.
 * Konfiguracja jest wczytywana bezpośrednio z pliku "package.json" znajdującego w katalogu projektu gry,
 * dzięki czemu niniejszy szablon można stosować poprzez proste osadzenie go z centralnego repozytorium NPM.
 */
let config = {
    title: "Phaser3 Game",
    gameWidth: 960,
    gameHeight: 540,
    gameMinScaleFactor: 1,
    gameMaxScaleFactor: 4,
    distDirName: "dist",
    htmlDirName: "src/html",
    assetsDirName: "src/assets",
    mainFileName: null,
    scenes: {
        GameScene: "src/scenes/GameScene.js"
    },
    startScene: "GameScene"
};
for (let configDirPath of [__dirname, rootPath]) {
    if (fs.existsSync(path.resolve(configDirPath, "package.json"))) {
        let packageJson = require(path.resolve(configDirPath, "package.json"));
        if (packageJson.config && packageJson.config.phaser3Builder) {
            config = Object.assign(config, packageJson.config.phaser3Builder);
        }
    }
}
const distDirPath = path.resolve(path.join(rootPath, config.distDirName));
const htmlDirPath = path.resolve(path.join(rootPath, config.htmlDirName));
const assetsDirPath = path.resolve(path.join(rootPath, config.assetsDirName));
const mainFilePath = config.mainFileName === null
    ? path.resolve(path.join(__dirname, "src", "main.js"))
    : path.resolve(path.join(rootPath, config.mainFileName));
const phaserFilePath = path.resolve(path.join(rootPath, "node_modules/phaser/dist/phaser.min.js"));

const scenesImports = [];
const scenesDeclarations = [];
for (let [sceneName, sceneFilePath] of Object.entries(config.scenes)) {
    sceneFilePath = path.resolve(path.join(rootPath, sceneFilePath)).replace(/\\/g, "/");
    scenesImports.push(`import ${sceneName} from "${sceneFilePath}"`);
    scenesDeclarations.push(`this.scene.add("${sceneName}", ${sceneName}, ${sceneName === config.startScene ? 'true' : 'false'});`);
}


/**
 * Usuwa katalog z zbudowaną grą.
 */
gulp.task("clean", () => gulp.src(path.join(rootPath, config.distDirName), { allowEmpty: true }).pipe(clean({ force: true })));

/**
 * Kopiuje pliki wymagane do uruchomienia gry.
 */
gulp.task("copy-html-default", () =>
    gulp.src(path.join(__dirname, "src", "html", "**", "*"))
        .pipe(gulpIf(file => file.extname === ".html", replace('{{BUILD_TIMESTAMP}}', Date.now())))
        .pipe(gulpIf(file => file.extname === ".html", replace('{{GAME_TITLE}}', config.title)))
        .pipe(gulp.dest(distDirPath))
);
/**
 * Kopiuje pliki wymagane do uruchomienia gry z dodatkowego katalogu.
 */
gulp.task("copy-html", () =>
    gulp.src(path.join(htmlDirPath, "**", "*"))
        .pipe(gulpIf(file => file.extname === ".html", replace('{{BUILD_TIMESTAMP}}', Date.now())))
        .pipe(gulpIf(file => file.extname === ".html", replace('{{GAME_TITLE}}', config.title)))
        .pipe(gulp.dest(distDirPath))
);

/**
 * Kopiuje pliki zasobów gry.
 */
gulp.task("copy-assets", () => gulp.src(path.join(assetsDirPath, "**", "*")).pipe(gulp.dest(distDirPath)));

/**
 * Kopiuje bibliotekę Phaser3 do katalogu z zbudowaną grą.
 */
gulp.task("copy-phaser", () => gulp.src(phaserFilePath).pipe(gulp.dest(distDirPath)));

/**
 * Buduje kod gry z wykorzystaniem nowych standardów języka JavaScript (w tym moduły) poprzez Babel.
 */
gulp.task("browserify-scripts", () =>
    browserify({
        entries: mainFilePath,
        debug: true,
        transform: [
            [
                browserifyReplace, {
                    replace: [
                        { from: "//{{SCENES_IMPORTS_WILL_BE_GENERATED_HERE}}", to: scenesImports.join("\n") },
                        { from: "//{{SCENES_DECLARATIONS_WILL_BE_GENERATED_HERE}}", to: scenesDeclarations.join("\n") }
                    ]
                }
            ],
            [
                babelify, {
                    presets: ["@babel/preset-env"]
                }
            ]
        ]
    })
    .bundle()
    .pipe(source('game.min.js'))
    .pipe(buffer())
    .pipe(replace('{{ENV}}', isProduction ? 'prod' : 'dev'))
    .pipe(replace("'{{GAME_WIDTH}}'", config.gameWidth))
    .pipe(replace("'{{GAME_HEIGHT}}'", config.gameHeight))
    .pipe(replace("'{{GAME_MAX_SCALE_FACTOR}}'", config.gameMaxScaleFactor))
    .pipe(replace("'{{GAME_MIN_SCALE_FACTOR}}'", config.gameMinScaleFactor))
    .pipe(gulpIf(isProduction === false, sourcemaps.init({ loadMaps: true })))
    .pipe(uglify())
    .pipe(gulpIf(isProduction === false, sourcemaps.write(".")))
    .pipe(gulp.dest(distDirPath))
);
 
/**
 * Budowanie gry poprzez wywołanie pojedynczych zadań w odpowiedniej kolejności.
 */
gulp.task("build", gulp.series("clean", "copy-html-default", "copy-html", "copy-assets", "copy-phaser", 'browserify-scripts'));

/**
 * Uruchomienie serwera wraz z aktualizacją plików gry w przypadku modyfikacji. 
 */
gulp.task('serve', () => {
    browserSync({
        server: {
            baseDir: distDirPath
        },
        open: false
    });
    gulp.watch(path.join(htmlDirPath, "**", "*").replace(/\\/g, "/"), gulp.series("copy-html")).on("change", browserSync.reload);
    gulp.watch(path.join(assetsDirPath, "**", "*").replace(/\\/g, "/"), gulp.series("copy-assets")).on("change", browserSync.reload);
    gulp.watch([
        path.join(rootPath, "**", "*.js").replace(/\\/g, "/"),
        "!" + path.join(rootPath, "vendor", "**", "*.js").replace(/\\/g, "/"),
        "!" + path.join(distDirPath, "**", "*.js").replace(/\\/g, "/"),
        "!" + path.join(rootPath, "gulpfile.js").replace(/\\/g, "/")
    ], gulp.series("copy-html-default", "copy-html", "browserify-scripts")).on("change", browserSync.reload);
})

/**
 * Domyślna komenda GULPa buduję grę oraz uruchamia serwer.
 */
gulp.task("default", gulp.series("build", "serve"));
