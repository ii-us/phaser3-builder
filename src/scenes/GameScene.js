export default class GameScene extends Phaser.Scene
{
    constructor ()
    {
        super("GameScene");
    }
    create ()
    {
        if (this.game.ENV === "dev") {
            console.log("GameScene has been started.");
        }
        this.cameras.main.setBackgroundColor("#002E5A");
    }
}
