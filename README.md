# Phaser3 Builder

Autor: _Arkadiusz Nowakowski_

Repozytorium ma na celu uproszczenie zajęć z zakresu tworzenia gier z wykorzystaniem silnika **Phaser3**.

## Jak używać niniejszego kodu?

W swoim projekcie gry należy zainstalować niniejszą bibliotekę za pomocą standardowej komendy `npm`-a:
```
npm i @lazarow/phaser3-builder
```
Dodatkowo, aby _VS Code_ podpowiadał prawidłowo metody _Phaser_-a należy doinstalować samą bibliotekę:
```
npm i phaser
```

Następnie należy przygotować **podstawowe** pliki gry: domyślną scenę gry oraz konfigurację. Biblioteka oparta jest o narzędzie `gulp`, które
automatyzuje proces budowania gry za pomocą następujących kroków:
- kopiowanie plików HTML wymaganych do uruchomienia gry, w tym pliki: `index.html` oraz `favicon.ico`;
- kopiowanie zasobów gry umieszczonych w wyznaczonym folderze;
- zbudowanie skryptu gry wraz z _transpilacją_ za pomocą Babel-a (konwersja z nowych standardów JS-a do obsługiwanej przez przeglądarki wersji).

Gra wpiera skalowanie gry przez możliwość wczytania zasobów z przyrostkiem `@X`, gdzie `X` to aktualny współczynnik skalowania (np. `2` oznacza dwa razy więcej pikseli od rozmiaru bazowego). Pliki można importować przy pomocy dodatkowej metody `getScaleSuffix()`, dodanej do obiektu gry, np.:
```js
this.load.atlas(
    'sprites',
    'images/' + this.game.getScaleSuffix('spritesheet.png'),
    'images/' + this.game.getScaleSuffix('spritesheet.json')
);
```
Przykładowo, w przypadku gdy współczynnik skalowania wynosi `2` (dostępny przez własność `this.game.scaleFactor`) wówczas wczytane pliki to `images/spritesheet@2.png` oraz `images/spritesheet@2.json`.

Przez scenę mam na myśli klasę rozszerzającą
klasę `Phaser.Scene`, np.:
```js
export default class GameScene extends Phaser.Scene
{
    constructor ()
    {
        super("GameScene");
    }
    create ()
    {
        ...
    }
}
```

Konfiguracja powinna być umieszczona w pliku `package.json` pod kluczem `config: { phaser3Builder: { ... }`, tj.:
```
{
  "version": "1.0.0",
  "dependencies": {
    ...
  },
  "config": {
    "phaser3Builder": {
      "title": "Phaser3 Game",
      ...
    }
  }
}
```

### Konfiguracja

| Klucz | Opis |
| --- | --- |
| `title` | Domyślnie _Phaser3 Game_. Tytuł gry dodawany w znacznikach `<title>` w pliku `index.html` (o ile nie podmieniono tego pliku). |
| `gameWidth` | Szerokość gry w pikselach, domyślnie _960_. |
| `gameHeight` | Wysokość gry w pikselach, domyślnie _540_. |
| `gameMinScaleFactor` | Minimalny współczynnik skalowania, domyślnie _1_. |
| `gameMaxScaleFactor` | Maksymalny współczynnik skalowania, domyślnie _4_. |
| `distDirName` | Domyślnie _dist_. Nazwa katalogu, w którym znajdą się pliki zbudowanej gry. |
| `htmlDirName` | Domyślnie _src/html_. Nazwa katalogu, w którym znajdują się pliki HTML niezbędne do uruchomienia gry. Niemniej podstawowy katalog jest również kopiowany przez co można podmienić tylko część plików, np. wyłącznie `favicon.ico`.  |
| `assetsDirName` | Domyślnie _src/assets_. Katalog zawierający zasoby gry. |
| `mainFileName` | Ścieżka do pliku wejściowego dla skryptu gry. Może nie być definiowany wówczas wymagane jest zdefiniowanie scen. |
| `scenes` |  Obiekt zawierający deklarację scen gry. Zobacz opis poniżej. |
| `startScene` | Klucz pierwszej sceny gry, domyślnie _GameScene_. |

#### Obiekt deklarujący sceny

Obiekt składa się w klucza sceny i ścieżki do pliku js eksportującego scenę gry. Domyślna wartość tego pola to:
```
scenes: {
    GameScene: "src/scenes/GameScene.js"
}
```

### Uruchomienie

Biblioteka posiada dwa tryby uruchomienia:
- deweloperski (dodatkowo licznik FPS-ów oraz mapy dla skryptów do identyfikacji błędów w kodzie);
- produkcyjny.

Obecny tryb można odczytać w kodzie przy pomocy własności `this.game.ENV`, która przyjmuje odpowiednio wartości _dev_ i _prod_.

Uruchamiamy:
```
npx gulp --gulpfile "node_modules\@lazarow\phaser3-builder\gulpfile.js"
npx gulp --gulpfile "node_modules\@lazarow\phaser3-builder\gulpfile.js" --production
```

## Dziennik zmian

### [1.0.5] - 10.01.2022
- Dodanie informacji o doinstalowaniu Phaser-a.

### [1.0.4] - 10.01.2022
- Zamiana biblioteki `gulp-rimraf` na `gulp-clean` ze względu na błędy.

### [1.0.3] - 10.01.2022
- Poprawienie ścieżki do głównego pliku gry `main.js`.

### [1.0.2] - 10.01.2022
- Aktualizacja pliku `package.json`.

### [1.0.1] - 10.01.2022
- Poprawienie opisu.
- Dodanie opisu uruchomienia.

### [1.0.0] - 10.01.2022
- Stworzenie repozytorium na podstawie poprzednich wprawek programistycznych z biblioteką Phaser3.
